from django import forms

class Friend_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    name_attrs = {
        'type': 'text',
        'class': 'name-form',
        'placeholder': 'Masukan nama...',
    }
    url_attrs = {
        'type': 'text',
        'class': 'url-form',
        'pattern': '^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$',
        'placeholder': 'Masukan url... (ex: http://debug-blaster.herokuapp.com)',
    }

    name = forms.CharField(label='Nama', max_length=27, required=True, widget=forms.TextInput(attrs=name_attrs))
    url = forms.CharField(widget=forms.TextInput(attrs=url_attrs), max_length=27, required=True)
