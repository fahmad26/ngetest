from django.shortcuts import render
from .models import Data

# Create your views here.
response = {}

def __init__():
	response['name'] = "Rafif Iqbal Shaputra"
	response['birthday'] = "1 January 1998"
	response['gender'] = "Male"
	response['expertise'] = [{'exp' : 'Marketing'},{'exp' : 'Collector'},{'exp' : 'Public Speaking'}]
	response['description'] = "Aesthetique Sekali. Selalu mencoba menjadi yang terbaik apapun yang terjadi.\
								Meski badai dan topan menghadang, ku kan terus berlari, berlari.. lari.. ri.. rii.. riiii...."
	response['email'] = "rafif.doang@gmail.com"
	response['imageurl'] = "https://www.w3schools.com/howto/img_avatar.png"

	data = Data(name=response['name'], birthday=response['birthday'], gender=response['gender'], expertise=response['expertise'],\
			description=response['description'], email=response['email'], imageurl=response['imageurl'])
	data.save()

__init__()
def index(request):
	html = 'app_2/app_2.html'
	return render(request, html, response)