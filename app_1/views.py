from django.shortcuts import render
from django.http import HttpResponseRedirect

from app_3.models import Friend

from .forms import Status_Form
from .models import Status

# Create your views here.

response = {}
def index(request):
	
    response['author'] = "Rafif Iqbal Shaputra" #TODO Implement yourname
    status= Status.objects.all()
    count_status=Status.objects.all().count()
    friend= Friend.objects.all().count()
    response['friend']=friend
    response['status'] = status[::-1]
    response['count_status']=count_status
	
    html = 'app_1/app_1.html'
    response['status_form'] = Status_Form
	
    return render(request, html, response)

def add_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        status = Status(description=response['description'])
        status.save()
        return HttpResponseRedirect('/app-1/')
    else:
        return HttpResponseRedirect('/app-1/')
		

