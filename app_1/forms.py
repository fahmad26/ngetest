from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
   
    
    description_attrs = {
        'type': 'text',
        'cols': 35,
        'rows': 8,
        'class': 'status-form-textarea',
        'placeholder':'Apa yang anda pikirkan?...'
    }

    
    description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))
	